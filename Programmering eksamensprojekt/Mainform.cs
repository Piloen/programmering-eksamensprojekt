﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programmering_eksamensprojekt
{
    public partial class Mainform : Form
    {
        public Mainform()
        {
            InitializeComponent();
        }
        private void Mainform_Load(object sender, EventArgs e)
        {
            // Direkte kilde ukendt, men taget fra tidligere kode fra marcus, og modificeret til at passe til vores updowns
            txtM.Controls[0].Visible = false;
            txtC.Controls[0].Visible = false;
            txtDeltaT.Controls[0].Visible = false;

            txtM.Text = null;
            txtC.Text = null;
            txtDeltaT.Text = null;
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            float m, c, deltaT;

            m = float.Parse(txtM.Text);
            c = float.Parse(txtC.Text);
            deltaT = float.Parse(txtDeltaT.Text);

            float q = m * c * deltaT;
            lblResult.Text = q.ToString();
        }


    }
}
