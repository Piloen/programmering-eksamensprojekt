﻿namespace Programmering_eksamensprojekt
{
    partial class Mainform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainform));
            this.grpCalc = new System.Windows.Forms.GroupBox();
            this.txtDeltaT = new System.Windows.Forms.NumericUpDown();
            this.txtC = new System.Windows.Forms.NumericUpDown();
            this.txtM = new System.Windows.Forms.NumericUpDown();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblM = new System.Windows.Forms.Label();
            this.lblC = new System.Windows.Forms.Label();
            this.lblDeltaT = new System.Windows.Forms.Label();
            this.lblQ = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblRelevantDescription = new System.Windows.Forms.Label();
            this.lblRelevant = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.grpCalc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeltaT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM)).BeginInit();
            this.SuspendLayout();
            // 
            // grpCalc
            // 
            this.grpCalc.Controls.Add(this.txtDeltaT);
            this.grpCalc.Controls.Add(this.txtC);
            this.grpCalc.Controls.Add(this.txtM);
            this.grpCalc.Controls.Add(this.lblResult);
            this.grpCalc.Controls.Add(this.lblM);
            this.grpCalc.Controls.Add(this.lblC);
            this.grpCalc.Controls.Add(this.lblDeltaT);
            this.grpCalc.Controls.Add(this.lblQ);
            this.grpCalc.Controls.Add(this.btnCalculate);
            this.grpCalc.Location = new System.Drawing.Point(12, 320);
            this.grpCalc.Name = "grpCalc";
            this.grpCalc.Size = new System.Drawing.Size(242, 118);
            this.grpCalc.TabIndex = 5;
            this.grpCalc.TabStop = false;
            this.grpCalc.Text = " ";
            // 
            // txtDeltaT
            // 
            this.txtDeltaT.DecimalPlaces = 2;
            this.txtDeltaT.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtDeltaT.Location = new System.Drawing.Point(55, 67);
            this.txtDeltaT.Maximum = new decimal(new int[] {
            65565,
            0,
            0,
            0});
            this.txtDeltaT.Name = "txtDeltaT";
            this.txtDeltaT.Size = new System.Drawing.Size(100, 20);
            this.txtDeltaT.TabIndex = 11;
            // 
            // txtC
            // 
            this.txtC.DecimalPlaces = 2;
            this.txtC.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtC.Location = new System.Drawing.Point(55, 41);
            this.txtC.Maximum = new decimal(new int[] {
            65565,
            0,
            0,
            0});
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(100, 20);
            this.txtC.TabIndex = 10;
            // 
            // txtM
            // 
            this.txtM.DecimalPlaces = 2;
            this.txtM.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtM.Location = new System.Drawing.Point(54, 15);
            this.txtM.Maximum = new decimal(new int[] {
            65565,
            0,
            0,
            0});
            this.txtM.Name = "txtM";
            this.txtM.Size = new System.Drawing.Size(100, 20);
            this.txtM.TabIndex = 6;
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(52, 95);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 9;
            // 
            // lblM
            // 
            this.lblM.AutoSize = true;
            this.lblM.Location = new System.Drawing.Point(40, 17);
            this.lblM.Name = "lblM";
            this.lblM.Size = new System.Drawing.Size(15, 13);
            this.lblM.TabIndex = 8;
            this.lblM.Text = "m";
            // 
            // lblC
            // 
            this.lblC.AutoSize = true;
            this.lblC.Location = new System.Drawing.Point(42, 43);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(13, 13);
            this.lblC.TabIndex = 7;
            this.lblC.Text = "c";
            // 
            // lblDeltaT
            // 
            this.lblDeltaT.AutoSize = true;
            this.lblDeltaT.Location = new System.Drawing.Point(38, 69);
            this.lblDeltaT.Name = "lblDeltaT";
            this.lblDeltaT.Size = new System.Drawing.Size(17, 13);
            this.lblDeltaT.TabIndex = 2;
            this.lblDeltaT.Text = "Δt";
            // 
            // lblQ
            // 
            this.lblQ.AutoSize = true;
            this.lblQ.Location = new System.Drawing.Point(40, 95);
            this.lblQ.Name = "lblQ";
            this.lblQ.Size = new System.Drawing.Size(15, 13);
            this.lblQ.TabIndex = 1;
            this.lblQ.Text = "Q";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(161, 14);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 98);
            this.btnCalculate.TabIndex = 0;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblRelevantDescription
            // 
            this.lblRelevantDescription.AutoSize = true;
            this.lblRelevantDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblRelevantDescription.Location = new System.Drawing.Point(387, 34);
            this.lblRelevantDescription.Name = "lblRelevantDescription";
            this.lblRelevantDescription.Size = new System.Drawing.Size(401, 391);
            this.lblRelevantDescription.TabIndex = 9;
            this.lblRelevantDescription.Text = resources.GetString("lblRelevantDescription.Text");
            this.lblRelevantDescription.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRelevant
            // 
            this.lblRelevant.AutoSize = true;
            this.lblRelevant.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.lblRelevant.Location = new System.Drawing.Point(642, 9);
            this.lblRelevant.Name = "lblRelevant";
            this.lblRelevant.Size = new System.Drawing.Size(146, 25);
            this.lblRelevant.TabIndex = 8;
            this.lblRelevant.Text = "Relevant formel";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblDescription.Location = new System.Drawing.Point(16, 48);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(294, 255);
            this.lblDescription.TabIndex = 7;
            this.lblDescription.Text = resources.GetString("lblDescription.Text");
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(253, 39);
            this.lblTitle.TabIndex = 6;
            this.lblTitle.Text = "Varmekapacitet";
            // 
            // Mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblRelevantDescription);
            this.Controls.Add(this.lblRelevant);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.grpCalc);
            this.Name = "Mainform";
            this.Text = "p";
            this.Load += new System.EventHandler(this.Mainform_Load);
            this.grpCalc.ResumeLayout(false);
            this.grpCalc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeltaT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCalc;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblM;
        private System.Windows.Forms.Label lblC;
        private System.Windows.Forms.Label lblDeltaT;
        private System.Windows.Forms.Label lblQ;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.NumericUpDown txtM;
        private System.Windows.Forms.NumericUpDown txtDeltaT;
        private System.Windows.Forms.NumericUpDown txtC;
        private System.Windows.Forms.Label lblRelevantDescription;
        private System.Windows.Forms.Label lblRelevant;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblTitle;
    }
}

